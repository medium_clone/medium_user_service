package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/medium_clone/medium_user_service/storage/repo"
)

func createUser(t *testing.T) *repo.User {
	u, err := strg.User().Create(&repo.User{
		FirstName:       faker.FirstName(),
		LastName:        faker.LastName(),
		PhoneNumber:     faker.Phonenumber(),
		Email:           faker.Email(),
		Gender:          "male",
		Password:        faker.Password(),
		Username:        faker.FirstName(),
		ProfileImageUrl: faker.URL(),
		Type:            repo.UserTypeUser,
	})
	require.NotEmpty(t, u)
	require.NoError(t, err)

	return u
}

func deleteUser(t *testing.T, Id int) {
	err := strg.User().Delete(&repo.DeleteUserRequest{Id: int64(Id)})
	require.NoError(t, err)
}

func TestCreateUser(t *testing.T) {
	user := createUser(t)
	err := strg.User().Delete(&repo.DeleteUserRequest{Id: user.ID})
	require.NoError(t, err)
	deleteUser(t, int(user.ID))
}

func TestGetUser(t *testing.T) {
	user := createUser(t)
	usr, err := strg.User().Get(user.ID)
	require.NoError(t, err)
	require.NotEmpty(t, usr)
	err = strg.User().Delete(&repo.DeleteUserRequest{Id: user.ID})
	require.NoError(t, err)
	deleteUser(t, int(user.ID))
}

func TestGetAllUser(t *testing.T) {
	num, err := faker.RandomInt(100)
	require.NoError(t, err)
	users, err := strg.User().GetAll(&repo.GetAllUsersParams{
		Limit: int32(num[0]),
		Page:  1,
	})
	require.NoError(t, err)
	require.NotEmpty(t, users)
}

func TestUpdateUser(t *testing.T) {
	user := createUser(t)
	newUser, err := strg.User().Update(&repo.UpdateUser{
		Id:              user.ID,
		FirstName:       faker.Name(),
		LastName:        faker.LastName(),
		PhoneNumber:     faker.Phonenumber(),
		Gender:          "male",
		ProfileImageUrl: faker.URL(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, newUser)
	err = strg.User().Delete(&repo.DeleteUserRequest{Id: user.ID})
	require.NoError(t, err)
	deleteUser(t, int(user.ID))
}

func TestDeleteUser(t *testing.T) {
	user := createUser(t)
	deleteUser(t, int(user.ID))
}
